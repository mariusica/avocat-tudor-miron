$(document).ready(function(){
	$('a[href*=#]').click(function() {
		if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
			var currentHash = this.hash;
			var target = $(currentHash);
			target = target.length ? target : $('body');
			if (target.length) {
				$('html,body').animate({
					scrollTop: target.offset().top
				}, 1000, 'swing', function(){
					window.location.hash = currentHash;
				});
				return false;
			}
		}
	});
});